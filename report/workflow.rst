After merging same raw samples_id.samples_rq from each line, Trimming step with fastp (https://github.com/OpenGene/fastp), Kallisto quant step .
After adapter removal with `Fastp <https://github.com/OpenGene/fastp>`_, transcripts were quantified with `Kallisto <https://pachterlab.github.io/kallisto/>`_.
For sample metadata, see {{ snakemake.config["samples"] }}_.
